-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 13, 2018 at 10:22 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siagatrans`
--

-- --------------------------------------------------------

--
-- Table structure for table `armada`
--

CREATE TABLE `armada` (
  `id_truck` int(11) NOT NULL,
  `jenis_truck` varchar(20) NOT NULL,
  `banyak_truck` int(3) NOT NULL,
  `harga` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `armada`
--

INSERT INTO `armada` (`id_truck`, `jenis_truck`, `banyak_truck`, `harga`) VALUES
(1, 'Colt Diesel Engkel', 10, 250000),
(2, 'Colt Diesel Double', 10, 300000),
(3, 'Fuso', 10, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_order_kurir`
--

CREATE TABLE `detail_order_kurir` (
  `id_order_kurir` bigint(10) NOT NULL,
  `isiKiriman` varchar(100) NOT NULL,
  `berat` int(3) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `namaPengirim` varchar(100) NOT NULL,
  `alamatPengirim` varchar(100) NOT NULL,
  `kontakPengirim` varchar(12) NOT NULL,
  `namaPenerima` varchar(100) NOT NULL,
  `alamatPenerima` varchar(100) NOT NULL,
  `kontakPenerima` varchar(100) NOT NULL,
  `id_order` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_order_sewa_truck`
--

CREATE TABLE `detail_order_sewa_truck` (
  `id_order_sewa_truck` int(11) NOT NULL,
  `jenisMuatan` varchar(100) NOT NULL,
  `jenisTruck` varchar(30) NOT NULL,
  `jumlahUnit` int(3) NOT NULL,
  `lokasiBarang` varchar(100) NOT NULL,
  `namaPengirim` varchar(100) NOT NULL,
  `alamatPengirim` varchar(100) NOT NULL,
  `kontakPengirim` varchar(12) NOT NULL,
  `namaPenerima` varchar(100) NOT NULL,
  `alamatPenerima` varchar(100) NOT NULL,
  `kontakPenerima` varchar(12) NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` varchar(10) NOT NULL,
  `asal` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `jarak` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `asal`, `tujuan`, `jarak`) VALUES
('BDG1', 'Bandung', 'Jakarta', 350),
('BDG2', 'Bandung', 'Surabaya', 400),
('JKT1', 'Jakarta', 'Bandung', 350),
('JKT2', 'Jakarta', 'Surabaya', 50),
('SBY1', 'Surabaya', 'Bandung', 400),
('SBY2', 'Surabaya', 'Jakarta', 50);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `nama_member` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `email`, `alamat`, `nama_member`, `phone`, `tgl_lahir`, `id_user`) VALUES
(1, 'defaekaardio@gmail.com', 'Jln Dipatiukur no 53 Rt01 Rw01', 'Defa Eka Ardio', '081225442323', '8/2/1997', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_order` bigint(10) NOT NULL,
  `pilihan_order` varchar(50) NOT NULL,
  `asal_barang` varchar(100) NOT NULL,
  `tujuan_barang` varchar(100) NOT NULL,
  `tanggal_order` varchar(30) NOT NULL,
  `id_member` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pricesing`
--

CREATE TABLE `pricesing` (
  `id_price` bigint(10) NOT NULL,
  `harga` bigint(10) NOT NULL,
  `id_order` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('benar','salah') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `id_user`, `password`, `status`) VALUES
('defa', 1, 'defa', 'benar');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `armada`
--
ALTER TABLE `armada`
  ADD PRIMARY KEY (`id_truck`);

--
-- Indexes for table `detail_order_kurir`
--
ALTER TABLE `detail_order_kurir`
  ADD PRIMARY KEY (`id_order_kurir`);

--
-- Indexes for table `detail_order_sewa_truck`
--
ALTER TABLE `detail_order_sewa_truck`
  ADD PRIMARY KEY (`id_order_sewa_truck`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `pricesing`
--
ALTER TABLE `pricesing`
  ADD PRIMARY KEY (`id_price`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order_sewa_truck`
--
ALTER TABLE `detail_order_sewa_truck`
  MODIFY `id_order_sewa_truck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1932305427;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
