<?php
	include_once "koneksi.php";
	
	$username = $_POST['username'];
	$jenisMuatan 	= $_POST['jenis_muatan'];
	$lokasiBarang = $_POST['lokasi_barang'];
	$namaPengirim = $_POST['nama_pengirim'];
	$alamatPengirim = $_POST['alamat_pengirim'];
	$kontakPengirim = $_POST['kontak_pengirim'];
	$namaPenerima = $_POST['nama_penerima'];
	$alamatPenerima = $_POST['alamat_penerima'];
	$kotaAsal = $_POST['kota_asal'];
	$kotaTujuan = $_POST['kota_tujuan'];
	$kontakPenerima = $_POST['kontak_penerima'];
	$jenisTruck = $_POST['jenis_truck'];
	$jumlahTruck = $_POST['jumlah_truck'];
	
	class usr{}
	
	if (empty($jenisMuatan) || empty($lokasiBarang) || empty($namaPengirim)
	|| empty($alamatPengirim) || empty($kontakPengirim) || empty($namaPenerima) || empty($alamatPenerima)
	|| empty($kotaAsal) || empty($kotaTujuan) || empty($kontakPenerima) || empty($jenisTruck) || empty($jumlahTruck)) { 
		$response = new usr();
		$response->success = 0;
		$response->message = "Pastikan kolom tidak boleh kosong"; 
		die(json_encode($response));
	} else {
		
		$queryIdMember = mysqli_query($connect,"SELECT id_member FROM member JOIN user USING(id_user) WHERE username='$username'");
		
		if($queryIdMember){
			$rowMember = mysqli_fetch_array($queryIdMember);
			$idMember = $rowMember['id_member'];
		}
		
		$queryCekHargaTruck = mysqli_query($connect, "SELECT harga FROM armada WHERE jenis_truck LIKE '%$jenisTruck%'");
		
		if($queryCekHargaTruck){
			$rowHarga = mysqli_fetch_array($queryCekHargaTruck);
			$hargaTruck = $rowHarga['harga'] * $jumlahTruck;
		}
		
		$queryCekHargaJarak = mysqli_query($connect, "SELECT jarak FROM lokasi WHERE asal LIKE '%$kotaAsal%' AND tujuan LIKE '%$kotaTujuan%';");
		
		if($queryCekHargaJarak){
			$rowHarga = mysqli_fetch_array($queryCekHargaJarak);
			$hargaJarak = $rowHarga['jarak'] * 50;
		}
		
		$hargaTotal = $hargaTruck + $hargaJarak; 
		
		
		$dateOrder = date('d/m/Y');
		
		$randnum = rand(1000000000,2000000000);
		
		$num_rows = mysqli_num_rows(mysqli_query($connect, "SELECT * FROM orders WHERE id_order ='$randnum'"));
		
		while ($num_rows != 0){
			
		$randnum2 = rand(1000000000,2000000000);
		
		$num_rows = mysqli_num_rows(mysqli_query($connect, "SELECT * FROM orders WHERE id_order ='$randnum2'"));
		}
				
		if ($num_rows == 0){
			
			$randnum2 = rand(1000000000,2000000000);
			$rows = $randnum;
			$rows2 = $randnum2;
			$idPrice = rand(1000000000,2000000000);
		}
		
		
		$query = mysqli_query($connect, "INSERT INTO orders (id_order, pilihan_order, asal_barang, tujuan_barang, tanggal_order, id_member) VALUES('".$rows."','SEWA_TRUCK','".$kotaAsal."','".$kotaTujuan."','".$dateOrder."','".$idMember."')");
		$query1 = mysqli_query($connect, "INSERT INTO detail_order_sewa_truck (id_order_sewa_truck, jenisMuatan, jenisTruck, jumlahUnit, lokasiBarang, namaPengirim, alamatPengirim, kontakPengirim, namaPenerima, alamatPenerima, kontakPenerima, id_order) VALUES ('".$rows2."','".$jenisMuatan."','".$jenisTruck."','".$jumlahTruck."','".$lokasiBarang."','".$namaPengirim."','".$alamatPengirim."','".$kontakPengirim."','".$namaPenerima."','".$alamatPenerima."','".$kontakPenerima."',$rows)");
		$query3 = mysqli_query($connect, "INSERT INTO pricesing (id_price,harga,id_order) VALUES ('".$idPrice."','".$hargaTotal."','".$rows."')");

	 		if ($query && $query1 && $query3){
	 			$response = new usr();
	 			$response->success = 1;
	 			$response->message = "Data sewa truck berhasil diinputkan.";
	 			$response->resi = $rows;
	 			die(json_encode($response));

	 		} else {
	 			$response = new usr();
	 			$response->success = 0;
	 			$response->message = "Gagal input data sewa kurir";
	 			die(json_encode($response));
	 		}
		
	}
	
	mysqli_close($connect);
?>
