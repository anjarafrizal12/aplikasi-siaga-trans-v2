<?php
	include_once "koneksi.php";

	class usr{}
	
	$resi = $_POST['resi'];
	
	if (empty($resi)) { 
		$response = new usr();
		$response->success = 0;
		$response->message = "Pastikan kolom resi tidak kosong"; 
		die(json_encode($response));
	}
	
	$queryKurir = mysqli_query($connect, "SELECT * FROM orders JOIN pricesing USING(id_order) JOIN detail_order_kurir USING(id_order) WHERE id_order='$resi'");
	
	$rowKurir = mysqli_fetch_array($queryKurir);
	
	
	if (!empty($rowKurir)){
		$response = new usr();
		$response->success = 1;
		$response->message = "Data Resi Kurir ditemukan";
		$response->id_order = $rowKurir['id_order'];
		$response->pilihan_order = $rowKurir['pilihan_order'];
		$response->asal_barang = $rowKurir['asal_barang'];
		$response->tujuan_barang = $rowKurir['tujuan_barang'];
		$response->tanggal_order = $rowKurir['tanggal_order'];
		$response->harga = $rowKurir['harga'];
		$response->isiKiriman = $rowKurir['isiKiriman'];
		$response->berat = $rowKurir['berat'];
		$response->lokasi = $rowKurir['lokasi'];
		$response->namaPengirim = $rowKurir['namaPengirim'];
		$response->alamatPengirim = $rowKurir['alamatPengirim'];
		$response->kontakPengirim = $rowKurir['kontakPengirim'];
		$response->namaPenerima = $rowKurir['namaPenerima'];
		$response->alamatPenerima = $rowKurir['alamatPenerima'];
		$response->kontakPenerima = $rowKurir['kontakPenerima'];
		
		die(json_encode($response));
		
	} else { 
		
		$queryTruck = mysqli_query($connect, "SELECT * FROM orders JOIN pricesing USING(id_order) JOIN detail_order_sewa_truck USING(id_order) WHERE id_order='$resi'");
	
		$rowTruck = mysqli_fetch_array($queryTruck);
		
		if (!empty($rowTruck)){
		$response = new usr();
		$response->success = 1;
		$response->message = "Data Resi Truck ditemukan";
		$response->id_order = $rowTruck['id_order'];
		$response->pilihan_order = $rowTruck['pilihan_order'];
		$response->asal_barang = $rowTruck['asal_barang'];
		$response->tujuan_barang = $rowTruck['tujuan_barang'];
		$response->tanggal_order = $rowTruck['tanggal_order'];
		$response->harga = $rowTruck['harga'];
		$response->jenisMuatan = $rowTruck['jenisMuatan'];
		$response->jenisTruck = $rowTruck['jenisTruck'];
		$response->jumlahUnit = $rowTruck['jumlahUnit'];
		$response->lokasiBarang = $rowTruck['lokasiBarang'];
		$response->namaPengirim = $rowTruck['namaPengirim'];
		$response->alamatPengirim = $rowTruck['alamatPengirim'];
		$response->kontakPengirim = $rowTruck['kontakPengirim'];
		$response->namaPenerima = $rowTruck['namaPenerima'];
		$response->alamatPenerima = $rowTruck['alamatPenerima'];
		$response->kontakPenerima = $rowTruck['kontakPenerima'];
		
		die(json_encode($response));
		
	} else { 
		
		$response = new usr();
		$response->success = 0;
		$response->message = "Data resi tidak ditemukan";
	 	die(json_encode($response));
	}
}
	
	mysqli_close($connect);

?>

