<?php
	include_once "koneksi.php";

	class usr{}
	
	$asal = $_POST['asal'];
	$tujuan = $_POST['tujuan'];
	
	if ((empty($asal)) || (empty($tujuan))) { 
		$response = new usr();
		$response->success = 0;
		$response->message = "Pastikan kolom tidak kosong"; 
		die(json_encode($response));
	}
	
	$query = mysqli_query($connect, "SELECT jarak FROM lokasi WHERE asal LIKE '%$asal%' AND tujuan LIKE '%$tujuan%';");
	
	$row = mysqli_fetch_array($query);
	
	
	if (!empty($row)){
		$response = new usr();
		$response->success = 1;
		$response->message = "Data lokasi ditemukan";
		$response->jarak = $row['jarak'];
		die(json_encode($response));
		
	} else { 
		$response = new usr();
		$response->success = 0;
		$response->message = "Data lokasi tidak ditemukan";
	 	die(json_encode($response));
	}
	
	mysqli_close($connect);

?>
