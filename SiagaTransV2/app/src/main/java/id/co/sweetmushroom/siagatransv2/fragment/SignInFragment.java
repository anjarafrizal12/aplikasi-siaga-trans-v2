package id.co.sweetmushroom.siagatransv2.fragment;


import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.app.AppController;
import id.co.sweetmushroom.siagatransv2.database.DatabaseHelper;
import id.co.sweetmushroom.siagatransv2.model.User;
import id.co.sweetmushroom.siagatransv2.utility.Server;

public class SignInFragment extends Fragment implements OnBackPressedListener {

    private Fragment mainMenu;
    private TextView signUp,signIn;
    private LinearLayout linear1,linear2;

    //Session
    boolean login = false;
    Handler handler = new Handler();

    //volley server
    int success;
    ConnectivityManager connectivityManager;

    private String urlSignIn = Server.URL + "signIn.php";
    private String urlSignUp = Server.URL + "signUp.php";

    private static final String TAG = SignInFragment.class.getSimpleName();

    String tag_json_object = "json_obj_req";

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username, email, alamat, phone, passw;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    ProgressDialog pDialog;
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id_user";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_PASSWORD = "password";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    //SignIn
    private EditText usernameSignIn,passwordSignIn;

    //SignUp
    private EditText usernameSignup,passwordSignUp,kpasswordSignUp,emailSignUp,
            phoneSignup,addressSignUp;

    public static TextView ttlSignUp;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sign_in, container, false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)

        {
            ((ViewGroup) v.findViewById(R.id.layoutSignIn)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(session_status, false);
        id = sharedpreferences.getString(TAG_ID, null);
        username = sharedpreferences.getString(TAG_USERNAME, null);
        phone = sharedpreferences.getString(TAG_PHONE, null);
        email = sharedpreferences.getString(TAG_EMAIL, null);
        passw = sharedpreferences.getString(TAG_PASSWORD, null);

        //Login
        usernameSignIn = v.findViewById(R.id.usernameSignIn);
        passwordSignIn = v.findViewById(R.id.passwordSignIn);
        Button btnLoginSignIn = v.findViewById(R.id.buttonSignIn);

        btnLoginSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameSignIn.getText().toString();
                String password = passwordSignIn.getText().toString();

                hideKeyboard(getContext());

                // mengecek kolom yang kosong
                if (username.trim().length() > 0 && password.trim().length() > 0) {
                    if (connectivityManager.getActiveNetworkInfo() != null
                            && connectivityManager.getActiveNetworkInfo().isAvailable()
                            && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        checkLogin(username, password);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity(), "Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });

        usernameSignup = v.findViewById(R.id.usernameSignUp);
        passwordSignUp = v.findViewById(R.id.passwordSignUp);
        kpasswordSignUp = v.findViewById(R.id.konfirmasiPasswordSignUp);
        ttlSignUp = v.findViewById(R.id.tglLahirSignUp);
        emailSignUp = v.findViewById(R.id.emailSignUp);
        phoneSignup = v.findViewById(R.id.phoneSignUp);
        addressSignUp = v.findViewById(R.id.addressSignUp);
        Button btnSignUp = v.findViewById(R.id.buttonSignUp);

        ttlSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameSignup.getText().toString();
                String password = passwordSignUp.getText().toString();
                String kpassword =  kpasswordSignUp.getText().toString();
                String ttl = ttlSignUp.getText().toString();
                String email = emailSignUp.getText().toString();
                String phone = phoneSignup.getText().toString();
                String address = addressSignUp.getText().toString();

                hideKeyboard(getContext());

                // mengecek kolom yang kosong
                if (username.trim().length() > 0 && password.trim().length() > 0
                        && kpassword.trim().length() > 0 && ttl.trim().length() > 0
                        && email.trim().length() > 0 && phone.trim().length() > 0
                        && address.trim().length() > 0) {
                    if (password.equals(kpassword)){
                        SignUp(username,password,ttl,email,phone,address);
                    } else {
                        Toast.makeText(getActivity(), "Password tidak cocok", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity(), "Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });

        mainMenu = new MainmenuFragment();
        signUp = v.findViewById(R.id.tv_signup);
        signIn = v.findViewById(R.id.tv_signin);
        linear1 = v.findViewById(R.id.linear1SignIn);
        linear2 = v.findViewById(R.id.linear2SignUp);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignIn();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignUp();
            }
        });

        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(mainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

    private void showSignIn(){
        linear1.setVisibility(View.VISIBLE);
        linear2.setVisibility(View.GONE);
        MainActivity.ma.setHeader("SIGN IN");
    }

    private void showSignUp(){
        linear1.setVisibility(View.GONE);
        linear2.setVisibility(View.VISIBLE);
        MainActivity.ma.setHeader("SIGN UP");
    }

    private void checkLogin(final String username, final String password) {

        User user = new User();

        user.setUsername("");
        user.setPassword("");

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait ...");
        showDialog();


        StringRequest strReq = new StringRequest(Request.Method.POST, urlSignIn, new Response.Listener<String>() {

            boolean status = false;

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        final String username = jObj.getString(TAG_USERNAME);
                        final String id = jObj.getString(TAG_ID);
                        final String email = jObj.getString(TAG_EMAIL);
                        final String phone = jObj.getString(TAG_PHONE);
                        final String address = jObj.getString(TAG_ALAMAT);

                        Log.e("Successfully Login!", jObj.toString());

                        status = true;

                        Toast.makeText(getActivity().getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();


                        Runnable runnableSuccess = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();

                                // menyimpan login ke session
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putBoolean(MainActivity.session_status, true);
                                editor.putString(TAG_ID, id);
                                editor.putString(TAG_USERNAME, username);
                                editor.putString(TAG_EMAIL, email);
                                editor.putString(TAG_PHONE, phone);
                                editor.putString(TAG_ALAMAT, address);
                                editor.apply();

                                MainActivity.ma.changeStatusLogin(); // merubah status login
                                MainActivity.ma.checkStatusLogin();

                                clearText();

                                MainActivity.ma.setFragment(mainMenu,0); // method untuk memanggil mainmenu
                                MainActivity.ma.defaultHeader();
                            }
                        };

                        Runnable runnableFailed = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                                Toast.makeText(getActivity(), "Maaf Terjadi Kesalahan Saat Masuk", Toast.LENGTH_LONG).show();
                                clearText();
                            }
                        };

                        if (status) {
                            handler.postDelayed(runnableSuccess,1500);
                        } else {
                            handler.postDelayed(runnableFailed, 1500);
                        }

                    } else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        hideDialog();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);

    }

    public void SignUp(final String uname, final String pass, String ttl, final String email, final String phone, final String alamat) {

        User user = new User();

        user.setUsername("");
        user.setPassword("");

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait ...");
        showDialog();


        StringRequest strReq = new StringRequest(Request.Method.POST, urlSignUp, new Response.Listener<String>() {

            boolean status = false;

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        final String username = jObj.getString(TAG_USERNAME);
                        final String id = jObj.getString(TAG_ID);

                        Log.e("Successfully Login!", jObj.toString());

                        status = true;

                        Toast.makeText(getActivity().getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();


                        Runnable runnableSuccess = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();

                                // menyimpan login ke session
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putBoolean(MainActivity.session_status, true);
                                editor.putString(TAG_ID, id);
                                editor.putString(TAG_USERNAME, username);
                                editor.apply();

                                MainActivity.ma.changeStatusLogin(); // merubah status login
                                MainActivity.ma.checkStatusLogin();

                                clearText();

                                MainActivity.ma.setFragment(mainMenu,0); // method untuk memanggil mainmenu
                                MainActivity.ma.defaultHeader();
                            }
                        };

                        Runnable runnableFailed = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                                Toast.makeText(getActivity(), "Maaf Terjadi Kesalahan Saat Masuk", Toast.LENGTH_LONG).show();
                                clearText();
                            }
                        };

                        if (status) {
                            handler.postDelayed(runnableSuccess,1500);
                        } else {
                            handler.postDelayed(runnableFailed, 1500);
                        }

                    } else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        hideDialog();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private int userSize(){
        ArrayList arrayUser = new ArrayList<User>();

        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {

            Cursor cursor = databaseHelper.queryData("select * from user ORDER BY user_id");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        User item = new User();

                        item.setUserId(cursor.getString(0));
                        item.setUsername(cursor.getString(1));
                        item.setTtl(cursor.getString(2));
                        item.setEmail(cursor.getString(3));
                        item.setPassword(cursor.getString(4));

                        arrayUser.add(item);
                    } while (cursor.moveToNext());
                }
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return arrayUser.size();
    }

    //hide keyboard
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void clearText(){
        usernameSignIn.setText(null);
        passwordSignIn.setText(null);
        usernameSignup.setText(null);
        passwordSignUp.setText(null);
        kpasswordSignUp.setText(null);
        emailSignUp.setText(null);
        ttlSignUp.setText(null);
        phoneSignup.setText(null);
        addressSignUp.setText(null);

    }

    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }


        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            ttlSignUp.setText(day+"/"+month+"/"+year);
        }

    }

}
