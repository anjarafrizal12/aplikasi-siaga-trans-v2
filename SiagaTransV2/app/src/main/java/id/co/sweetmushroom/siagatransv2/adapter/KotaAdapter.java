package id.co.sweetmushroom.siagatransv2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.model.Kota;


public class KotaAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Kota> item;

    public KotaAdapter(Activity activity, List<Kota> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int location) {
        return item.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_kota, null);

        TextView pendidikan = (TextView) convertView.findViewById(R.id.kota);

        Kota data;
        data = item.get(position);

        pendidikan.setText(data.getNmKota());

        return convertView;
    }
}

