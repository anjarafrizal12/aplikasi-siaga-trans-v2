package id.co.sweetmushroom.siagatransv2;

public interface OnBackPressedListener {
    boolean onBackPressed();
}