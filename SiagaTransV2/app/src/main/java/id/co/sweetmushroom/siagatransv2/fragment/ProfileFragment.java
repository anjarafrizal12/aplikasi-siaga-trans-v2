package id.co.sweetmushroom.siagatransv2.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.database.DatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements OnBackPressedListener {

    private Fragment mainMenu;
    SharedPreferences sharedpreferences;
    Boolean session = false;
    private TextView uname,mail,phone,address;
    String nUname,nMail,nPhone,nAddress,nPass;

    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    ProgressDialog pDialog;
    DatabaseHelper databaseHelper;
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_PASSWORD = "password";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(MainActivity.session_status, true);
        nUname = sharedpreferences.getString(TAG_USERNAME, null);
        nMail = sharedpreferences.getString(TAG_EMAIL, null);
        nPhone = sharedpreferences.getString(TAG_PHONE, null);
        nAddress = sharedpreferences.getString(TAG_ALAMAT, null);
        nPass = sharedpreferences.getString(TAG_PASSWORD,null);

        mainMenu = new MainmenuFragment();

        uname = v.findViewById(R.id.tv_username);
        mail = v.findViewById(R.id.tv_mail);
        phone = v.findViewById(R.id.tv_phone);
        address = v.findViewById(R.id.tv_address);

        uname.setText(nUname);
        mail.setText(nMail);
        phone.setText(nPhone);
        address.setText(nAddress);

        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(mainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

}
