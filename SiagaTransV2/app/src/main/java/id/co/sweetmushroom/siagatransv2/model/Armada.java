package id.co.sweetmushroom.siagatransv2.model;

public class Armada {
    String jenisTruck,banyakTruck;

    public String getJenisTruck() {
        return jenisTruck;
    }

    public void setJenisTruck(String jenisTruck) {
        this.jenisTruck = jenisTruck;
    }

    public String getBanyakTruck() {
        return banyakTruck;
    }

    public void setBanyakTruck(String banyakTruck) {
        this.banyakTruck = banyakTruck;
    }
}
