package id.co.sweetmushroom.siagatransv2.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment implements OnBackPressedListener {

    private Fragment mainMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about_us, container, false);

        mainMenu = new MainmenuFragment();

        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(mainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

}
