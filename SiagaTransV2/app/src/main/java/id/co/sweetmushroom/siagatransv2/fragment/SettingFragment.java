package id.co.sweetmushroom.siagatransv2.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.database.DatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements OnBackPressedListener {

    Handler handler = new Handler();
    private Fragment mainMenu;
    SharedPreferences sharedpreferences;
    Boolean session = false;
    private TextView uname,mail,phone,address;
    private Button btnChangePassword;
    private EditText etPassLama,etPassBaru, etKpassBaru;
    String nUname,nMail,nPhone,nAddress,nPass;

    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    ProgressDialog pDialog;
    DatabaseHelper databaseHelper;
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_PASSWORD = "password";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(MainActivity.session_status, true);
        nUname = sharedpreferences.getString(TAG_USERNAME, null);
        nMail = sharedpreferences.getString(TAG_EMAIL, null);
        nPhone = sharedpreferences.getString(TAG_PHONE, null);
        nAddress = sharedpreferences.getString(TAG_ALAMAT, null);
        nPass = sharedpreferences.getString(TAG_PASSWORD, null);

        mainMenu = new MainmenuFragment();

        uname = v.findViewById(R.id.tv_usernameSetting);
        mail = v.findViewById(R.id.tv_mailSetting);
        phone = v.findViewById(R.id.tv_phoneSetting);
        address = v.findViewById(R.id.tv_addressSetting);

        uname.setText(nUname);
        mail.setText(nMail);
        phone.setText(nPhone);
        address.setText(nAddress);

        etPassLama = v.findViewById(R.id.et_passwordLama);
        etPassBaru = v.findViewById(R.id.et_passwordBaru);
        etKpassBaru = v.findViewById(R.id.et_kPasswordBaru);

        btnChangePassword = v.findViewById(R.id.buttonChangePassword);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passLama = etPassLama.getText().toString();
                String passBaru = etPassBaru.getText().toString();
                String konfirmPassBaru = etKpassBaru.getText().toString();

                hideKeyboard(getContext());

                // mengecek kolom yang kosong
                if (passLama.trim().length() > 0) {
                    if (passLama.equals(nPass)){
                        if (passBaru.trim().length() > 0){
                            if (passBaru.equals(nPass)){
                                Toast.makeText(getActivity(), "Password baru tidak boleh sama dengan password lama", Toast.LENGTH_LONG).show();
                            } else {
                                if (konfirmPassBaru.trim().length() > 0){
                                    if (konfirmPassBaru.equals(passBaru)){
                                        chagePassword(passBaru);
                                    } else {
                                        Toast.makeText(getActivity(), "Password baru tidak cocok", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Inputkan ulang password baru", Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Inputkan password baru", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Inputkan password lama anda", Toast.LENGTH_LONG).show();
                    }
                    hideKeyboard(getContext());
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity(), "Inputkan Password Lama Anda", Toast.LENGTH_LONG).show();
                    hideKeyboard(getContext());
                }
            }
        });

        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(mainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

    //hide keyboard
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void clearText(){
        etPassLama.setText(null);
        etPassBaru.setText(null);
        etKpassBaru.setText(null);
    }

    private void chagePassword(String password){

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait ...");
        showDialog();

        boolean success = false;
        String passBaru = "'"+password+"'";
        String uname = "'"+nUname+"'";
        databaseHelper = new DatabaseHelper(getContext());
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();

            db.execSQL("UPDATE user SET password="+passBaru+" WHERE username="+uname+"");

            success = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Runnable runnableSuccess = new Runnable() {
            @Override
            public void run() {
                hideDialog();

                Toast.makeText(getActivity(), "Password Berhasil Di Ubah", Toast.LENGTH_LONG).show();
                clearText();


            }
        };

        Runnable runnableFailed = new Runnable() {
            @Override
            public void run() {
                hideDialog();
                Toast.makeText(getActivity(), "Gagal merubah password", Toast.LENGTH_LONG).show();
                clearText();
            }
        };

        if (success){
            handler.postDelayed(runnableSuccess,1500);
        } else {
            handler.postDelayed(runnableFailed,1500);
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
