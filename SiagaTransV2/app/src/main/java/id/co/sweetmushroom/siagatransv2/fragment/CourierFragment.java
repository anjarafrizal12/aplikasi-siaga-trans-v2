package id.co.sweetmushroom.siagatransv2.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.adapter.KotaAdapter;
import id.co.sweetmushroom.siagatransv2.app.AppController;
import id.co.sweetmushroom.siagatransv2.database.DatabaseHelper;
import id.co.sweetmushroom.siagatransv2.model.Kota;
import id.co.sweetmushroom.siagatransv2.model.User;
import id.co.sweetmushroom.siagatransv2.utility.Server;


/**
 * A simple {@link Fragment} subclass.
 */
public class CourierFragment extends Fragment implements OnBackPressedListener {

    Fragment FragmentMainMenu;

    boolean login = false;
    Handler handler = new Handler();
    private String nomorResi;

    //volley server
    int success;
    ConnectivityManager connectivityManager;

    KotaAdapter kotaAdapterAsal,kotaAdapterTujuan;
    List<Kota> listKotaAsal = new ArrayList<Kota>();
    List<Kota> listKotaTujuan = new ArrayList<Kota>();
    String ktAsal,ktTujuan;

    private String urlInsertCourier = Server.URL + "InputInstantCourier.php";
    private String urlKota = Server.URL + "pilihKota.php";

    private static final String TAG = CourierFragment.class.getSimpleName();

    String nUname,nMail,nPhone,nAddress,nPass;
    String tag_json_object = "json_obj_req";
    public static final String TAG_KOTA = "kota";

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username, email, alamat, phone, passw;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    ProgressDialog pDialog;
    public final static String TAG_KIRIMAN = "isi_kiriman";
    public final static String TAG_BERAT = "berat";
    public final static String TAG_LOKASI = "lokasi";
    public final static String TAG_NPENGIRIM = "nama_pengirim";
    public final static String TAG_ALAMATPENGIRIM = "alamat_pengirim";
    public final static String TAG_KONTAKPENGIRIM = "kontak_pengirim";
    public final static String TAG_NAMAPENERIMA = "nama_penerima";
    public final static String TAG_ALAMATPENERIMA = "alamat_penerima";
    public final static String TAG_KOTATUJUAN = "kota_tujuan";
    public final static String TAG_KONTAKPENERIMA = "kontak_penerima";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_PASSWORD = "password";

    private EditText isiKiriman,berat,lokasi,nPengirim,alamatPengirim,kontakPengirim,namaPenerima,alamatPenerima,kontakPenerima;
    private Spinner kotaTujuan,kotaAsal;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_courier, container, false);

        FragmentMainMenu = new MainmenuFragment();

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        isiKiriman = v.findViewById(R.id.isiKiriman);
        berat = v.findViewById(R.id.berat);
        lokasi = v.findViewById(R.id.lokasi);
        nPengirim = v.findViewById(R.id.namaPengirimKurir);
        alamatPengirim = v.findViewById(R.id.alamatPengirimKurir);
        kontakPengirim = v.findViewById(R.id.kontakPengirimKurir);
        namaPenerima = v.findViewById(R.id.namaPenerimaKurir);
        alamatPenerima = v.findViewById(R.id.alamatPenerimaKurir);
        kotaTujuan = v.findViewById(R.id.kotaTujuanKurir);
        kotaAsal = v.findViewById(R.id.kotaAsalKurir);
        kontakPenerima = v.findViewById(R.id.kontakPenerimaKurir);

        kotaAsal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktAsal = listKotaAsal.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        kotaTujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktTujuan = listKotaTujuan.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(MainActivity.session_status, true);
        nUname = sharedpreferences.getString(TAG_USERNAME, null);
        nMail = sharedpreferences.getString(TAG_EMAIL, null);
        nPhone = sharedpreferences.getString(TAG_PHONE, null);
        nAddress = sharedpreferences.getString(TAG_ALAMAT, null);
        nPass = sharedpreferences.getString(TAG_PASSWORD,null);

        kotaAdapterAsal = new KotaAdapter(getActivity(), listKotaAsal);
        kotaAdapterTujuan = new KotaAdapter(getActivity(), listKotaTujuan);

        kotaAsal.setAdapter(kotaAdapterAsal);
        kotaTujuan.setAdapter(kotaAdapterTujuan);

        fetchKota();

        Button pesanKurir = v.findViewById(R.id.pesanKurir);

        pesanKurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String kiriman = isiKiriman.getText().toString();
                String beratBarang = berat.getText().toString();
                String lokasiBarang = lokasi.getText().toString();
                String pengirim = nPengirim.getText().toString();
                String addressPenngirim = alamatPengirim.getText().toString();
                String hpPengirim = kontakPengirim.getText().toString();
                String penerima = namaPenerima.getText().toString();
                String addressPenerima = alamatPenerima.getText().toString();
                String hpPenerima = kontakPenerima.getText().toString();

                hideKeyboard(getContext());

                // mengecek kolom yang kosong
                if (kiriman.trim().length() > 0 && beratBarang.trim().length() > 0 && lokasiBarang.trim().length() > 0
                        && pengirim.trim().length() > 0 && addressPenngirim.trim().length() > 0 && hpPengirim.trim().length() > 0
                        && penerima.trim().length() > 0 && addressPenerima.trim().length() > 0 && ktAsal.trim().length() > 0 && ktTujuan.trim().length() > 0
                        && hpPenerima.trim().length() > 0) {
                    if (connectivityManager.getActiveNetworkInfo() != null
                            && connectivityManager.getActiveNetworkInfo().isAvailable()
                            && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        inputKurir(kiriman,beratBarang,lokasiBarang,pengirim,addressPenngirim,hpPengirim,penerima,addressPenerima,ktAsal,ktTujuan,hpPenerima);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity(), "Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(FragmentMainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void inputKurir(final String kiriman,final String beratBarang, final String lokasiBarang, final String namaPengirim, final String alamatPengirim,
                            final String kontakPengirim,final String namaPenerima, final String alamatPenerima,final String kotaAsal,final String kotaTujuan,final String kontakPenerima){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Pesan Kurir ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, urlInsertCourier, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {

                        Log.e("Berhasil input kurir!", jObj.toString());

                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        clearText();
                        nomorResi = jObj.getString("resi");

                        showDialogPemberitahuan(kiriman,beratBarang,lokasiBarang,namaPengirim,alamatPengirim,kontakPengirim,namaPenerima,
                                alamatPenerima,kotaAsal,kotaTujuan,kontakPenerima);


                    } else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Input Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",nUname);
                params.put("isi_kiriman", kiriman);
                params.put("berat", beratBarang);
                params.put("lokasi", lokasiBarang);
                params.put("nama_pengirim", namaPengirim);
                params.put("alamat_pengirim", alamatPengirim);
                params.put("kontak_pengirim", kontakPengirim);
                params.put("nama_penerima", namaPenerima);
                params.put("alamat_penerima", alamatPenerima);
                params.put("kota_asal", kotaAsal);
                params.put("kota_tujuan", kotaTujuan);
                params.put("kontak_penerima", kontakPenerima);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);

    }

    private int userSize(){
        ArrayList arrayUser = new ArrayList<User>();

        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {

            Cursor cursor = databaseHelper.queryData("select * from user ORDER BY user_id");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        User item = new User();

                        item.setUserId(cursor.getString(0));
                        item.setUsername(cursor.getString(1));
                        item.setTtl(cursor.getString(2));
                        item.setEmail(cursor.getString(3));
                        item.setPassword(cursor.getString(4));

                        arrayUser.add(item);
                    } while (cursor.moveToNext());
                }
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return arrayUser.size();
    }

    //hide keyboard
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void clearText(){
        isiKiriman.setText("");
        berat.setText("");
        lokasi.setText("");
        nPengirim.setText("");
        alamatPengirim.setText("");
        kontakPengirim.setText("");
        namaPenerima.setText("");
        alamatPenerima.setText("");
        kontakPenerima.setText("");
    }

    private void fetchKota(){
        listKotaAsal.clear();
        listKotaTujuan.clear();

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");
        showDialog();

        // Creating volley request obj
        JsonArrayRequest jArrRequest = new JsonArrayRequest(urlKota,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        Kota awal = new Kota();
                        awal.setNmKota("Pilih Kota Asal");
                        listKotaAsal.add(awal);

                        Kota tujuan = new Kota();
                        tujuan.setNmKota("Pilih Kota Tujuan");
                        listKotaTujuan.add(tujuan);

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Kota item = new Kota();

                                item.setNmKota(obj.getString(TAG_KOTA));

                                listKotaAsal.add(item);
                                listKotaTujuan.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        kotaAdapterAsal.notifyDataSetChanged();
                        kotaAdapterTujuan.notifyDataSetChanged();

                        hideDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArrRequest);
    }

    private void showDialogPemberitahuan(final String kiriman,final String beratBarang, final String lokasiBarang, final String namaPengirim, final String alamatPengirim,
                                         final String kontakPengirim,final String namaPenerima, final String alamatPenerima,final String kotaAsal,final String kotaTujuan,final String kontakPenerima){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_pemberitahuan);
        dialog.setTitle("Pemberitahuan!");
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final ScrollView scrollCourier = dialog.findViewById(R.id.instantCourier);

        TextView resiX = dialog.findViewById(R.id.nomorResi);
        TextView isiKirimanX = dialog.findViewById(R.id.isiKirimanDialog);
        TextView beratX = dialog.findViewById(R.id.beratDialog);
        TextView lokasiX = dialog.findViewById(R.id.lokasiBarangDialog);
        TextView namaPengirimX = dialog.findViewById(R.id.namaPengirimDialog);
        TextView alamatPengirimX = dialog.findViewById(R.id.alamatPengirimDialog);
        TextView kotaPengirimX = dialog.findViewById(R.id.kotaAsalDialog);
        TextView kontakPengirimX = dialog.findViewById(R.id.kontakPengirimDialog);
        TextView namaPenerimaX = dialog.findViewById(R.id.namaPenerimaDialog);
        TextView alamatPenerimaX = dialog.findViewById(R.id.alamatpenerimaDialog);
        TextView kotaPenerimaX = dialog.findViewById(R.id.kotaTujuanDialog);
        TextView kontakPenerimaX = dialog.findViewById(R.id.kontakPenerimaDialog);

        Button btnOk = dialog.findViewById(R.id.btnOkDialog);

        resiX.setText(nomorResi);
        isiKirimanX.setText(kiriman);
        beratX.setText(beratBarang);
        lokasiX.setText(lokasiBarang);
        namaPengirimX.setText(namaPengirim);
        alamatPengirimX.setText(alamatPengirim);
        kotaPengirimX.setText(kotaAsal);
        kontakPengirimX.setText(kontakPengirim);
        namaPenerimaX.setText(namaPenerima);
        alamatPenerimaX.setText(alamatPenerima);
        kotaPenerimaX.setText(kotaTujuan);
        kontakPenerimaX.setText(kontakPenerima);

        scrollCourier.setVisibility(View.VISIBLE);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                scrollCourier.setVisibility(View.GONE);
                MainActivity.ma.setFragment(FragmentMainMenu,0);
                MainActivity.ma.defaultHeader();
            }
        });

        dialog.show();

    }

}
