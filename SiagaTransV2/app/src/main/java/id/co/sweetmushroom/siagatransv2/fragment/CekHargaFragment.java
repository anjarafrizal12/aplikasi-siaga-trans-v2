package id.co.sweetmushroom.siagatransv2.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.adapter.KotaAdapter;
import id.co.sweetmushroom.siagatransv2.app.AppController;
import id.co.sweetmushroom.siagatransv2.model.Kota;
import id.co.sweetmushroom.siagatransv2.utility.Server;


public class CekHargaFragment extends Fragment implements OnBackPressedListener {

    Fragment FragmentMainMenu;
    private String urlKota = Server.URL + "pilihKota.php";
    private String urlHarga = Server.URL + "cekHarga.php";

    Handler handler = new Handler();
    Spinner spinner_asal,spinner_tujuan;
    int success;
    ProgressDialog pDialog;
    KotaAdapter kotaAdapterAsal,kotaAdapterTujuan;
    List<Kota> listKotaAsal = new ArrayList<Kota>();
    List<Kota> listKotaTujuan = new ArrayList<Kota>();
    String ktAsal,ktTujuan;
    View view1;
    LinearLayout linear1;
    TextView kotaAsal,kotaTujuan,jarakKota,priceKota;

    ConnectivityManager connectivityManager;

    private static final String TAG = CekHargaFragment.class.getSimpleName();

    public static final String TAG_KOTA = "kota";
    public static final String TAG_JARAK = "jarak";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    String tag_json_object = "json_obj_req";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cek_harga, container, false);

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        view1 = v.findViewById(R.id.view1);
        linear1 = v.findViewById(R.id.linear1);

        kotaAsal = v.findViewById(R.id.ktAsal);
        kotaTujuan = v.findViewById(R.id.ktTujuan);
        jarakKota = v.findViewById(R.id.jarak);
        priceKota = v.findViewById(R.id.price);

        spinner_asal = v.findViewById(R.id.spinnerKotaAsal);
        spinner_tujuan = v.findViewById(R.id.spinnerKotaTujuan);

        FragmentMainMenu = new MainmenuFragment();

        spinner_asal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktAsal = listKotaAsal.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_tujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktTujuan = listKotaTujuan.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        kotaAdapterAsal = new KotaAdapter(getActivity(), listKotaAsal);
        kotaAdapterTujuan = new KotaAdapter(getActivity(), listKotaTujuan);

        spinner_asal.setAdapter(kotaAdapterAsal);
        spinner_tujuan.setAdapter(kotaAdapterTujuan);

        fetchKota();

        Button btnCekHarga = v.findViewById(R.id.btnCekHarga);

        btnCekHarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ktAsal.equals("Pilih Kota Asal") || ktTujuan.equals("Pilih Kota Tujuan") ) {
                    Toast.makeText(getActivity(), "Pilih Kota Asal dan Tujuan Terlebih Dahulu", Toast.LENGTH_LONG).show();
                } else {
                    if (connectivityManager.getActiveNetworkInfo() != null
                            && connectivityManager.getActiveNetworkInfo().isAvailable()
                            && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        cekHarga(ktAsal, ktTujuan);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        return v;
    }

    private void fetchKota(){
        listKotaAsal.clear();
        listKotaTujuan.clear();

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");
        showDialog();

        // Creating volley request obj
        JsonArrayRequest jArrRequest = new JsonArrayRequest(urlKota,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        Kota awal = new Kota();
                        awal.setNmKota("Pilih Kota Asal");
                        listKotaAsal.add(awal);

                        Kota tujuan = new Kota();
                        tujuan.setNmKota("Pilih Kota Tujuan");
                        listKotaTujuan.add(tujuan);

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Kota item = new Kota();

                                item.setNmKota(obj.getString(TAG_KOTA));

                                listKotaAsal.add(item);
                                listKotaTujuan.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        kotaAdapterAsal.notifyDataSetChanged();
                        kotaAdapterTujuan.notifyDataSetChanged();

                        hideDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArrRequest);
    }

    private void cekHarga(final String asal, final String tujuan){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait ...");
        showDialog();


        StringRequest strReq = new StringRequest(Request.Method.POST, urlHarga, new Response.Listener<String>() {

            boolean status = false;

            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        final String jarak = jObj.getString(TAG_JARAK);

                        Log.e("Successfully find location!", jObj.toString());

                        status = true;

                        Toast.makeText(getActivity().getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();


                        Runnable runnableSuccess = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();

                                double price = Double.parseDouble(jarak) * 50;

                                kotaAsal.setText(ktAsal);
                                kotaTujuan.setText(ktTujuan);
                                jarakKota.setText(jarak + " KM");
                                priceKota.setText(String.valueOf(price));

                                view1.setVisibility(View.VISIBLE);
                                linear1.setVisibility(View.VISIBLE);

                            }
                        };

                        Runnable runnableFailed = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                                Toast.makeText(getActivity(), "Maaf Kota Tidak Ditemukan", Toast.LENGTH_LONG).show();

                            }
                        };

                        if (status) {
                            handler.postDelayed(runnableSuccess,1500);
                        } else {
                            handler.postDelayed(runnableFailed, 1500);
                        }

                    } else {
                        kotaAsal.setText(ktAsal);
                        kotaTujuan.setText(ktTujuan);
                        jarakKota.setText("0 KM");
                        priceKota.setText("0.0");

                        view1.setVisibility(View.VISIBLE);
                        linear1.setVisibility(View.VISIBLE);
                        hideDialog();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("asal", asal);
                params.put("tujuan", tujuan);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);

    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(FragmentMainMenu,0);
        MainActivity.ma.defaultHeader();
        view1.setVisibility(View.GONE);
        linear1.setVisibility(View.GONE);
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
