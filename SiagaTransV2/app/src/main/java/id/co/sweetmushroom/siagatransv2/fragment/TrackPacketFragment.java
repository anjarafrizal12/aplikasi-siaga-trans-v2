package id.co.sweetmushroom.siagatransv2.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.adapter.KotaAdapter;
import id.co.sweetmushroom.siagatransv2.app.AppController;
import id.co.sweetmushroom.siagatransv2.model.Kota;
import id.co.sweetmushroom.siagatransv2.utility.Server;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrackPacketFragment extends Fragment implements OnBackPressedListener {

    Fragment FragmentMainMenu;
    EditText inputResi;

    TextView noResi,jenisOrder,kotaAsalX,kotaTujuanX,hargaKurir,tglKurir,isiKiriman,beratKurir,lokasiKurir,nmPengirim,alamatPengirimKurir,phonePengirimKurir
            ,nmPenerima,alamatPenerimaKurir,phonePenerimaKurir;

    TextView noResiTruck,jenisOrderTruck,kotaAsalXTruck,kotaTujuanXTruck,hargaTruck,lokasiBarangTruck,tglTruck,jenisMuatanTruck,jenisTruck,jumlahTruck,nmPengirimTruck,alamatPengirimTruck,phonePengirimTruck
            ,nmPenerimaTruck,alamatPenerimaTruck,phonePenerimaTruck;
    private String resi;

    private String urlResi = Server.URL + "cekResi.php";

    Handler handler = new Handler();
    int success;
    ProgressDialog pDialog;
    View view1;
    ScrollView scrollKurir,scrollTruck;

    ConnectivityManager connectivityManager;

    private static final String TAG = TrackPacketFragment.class.getSimpleName();

    public static final String TAG_ORDER = "id_order";
    public static final String TAG_PILIHAN = "pilihan_order";
    public static final String TAG_ASAL = "asal_barang";
    public static final String TAG_TUJUAN = "tujuan_barang";
    public static final String TAG_TGL = "tanggal_order";
    public static final String TAG_HARGA = "harga";
    public static final String TAG_ISI = "isiKiriman";
    public static final String TAG_BERAT = "berat";
    public static final String TAG_LOKASI = "lokasi";
    public static final String TAG_NMPENGIRIM = "namaPengirim";
    public static final String TAG_ALAMATPENGIRIM = "alamatPengirim";
    public static final String TAG_PHONEPENGIRIM = "kontakPengirim";
    public static final String TAG_NMPENERIMA = "namaPenerima";
    public static final String TAG_ALAMATPENERIMA = "alamatPenerima";
    public static final String TAG_PHONEPENERIMA = "kontakPenerima";
    public static final String TAG_JENISMUATAN = "jenisMuatan";
    public static final String TAG_JENISTRUCK = "jenisTruck";
    public static final String TAG_JUMLAHUNIT = "jumlahUnit";
    public static final String TAG_LOKASIBARANG = "lokasiBarang";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    String tag_json_object = "json_obj_req";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_track_packet, container, false);
        FragmentMainMenu = new MainmenuFragment();

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        noResi = v.findViewById(R.id.resiKurir);
        jenisOrder = v.findViewById(R.id.jenisKurir);
        kotaAsalX = v.findViewById(R.id.kotaAsalKurir);
        kotaTujuanX = v.findViewById(R.id.kotaTujuanKurir);
        hargaKurir = v.findViewById(R.id.hargaKurir);
        tglKurir = v.findViewById(R.id.tglOrderKurir);
        isiKiriman = v.findViewById(R.id.isiKurir);
        beratKurir = v.findViewById(R.id.beratKirimanKurir);
        lokasiKurir = v.findViewById(R.id.lokasiKurir);
        nmPengirim = v.findViewById(R.id.namaPengirimKurir);
        alamatPengirimKurir = v.findViewById(R.id.alamatPengirimKurir);
        phonePengirimKurir = v.findViewById(R.id.phonePengirimKurir);
        nmPenerima = v.findViewById(R.id.namaPenerimaKurir);
        alamatPenerimaKurir = v.findViewById(R.id.alamatPenerimaKurir);
        phonePenerimaKurir = v.findViewById(R.id.phonePenerimaKurir);

        noResiTruck = v.findViewById(R.id.resiTruck);
        jenisOrderTruck = v.findViewById(R.id.jenisOrderTruck);
        kotaAsalXTruck = v.findViewById(R.id.kotaAsalTruck);
        kotaTujuanXTruck = v.findViewById(R.id.kotaTujuanTruck);
        hargaTruck = v.findViewById(R.id.hargaTruck);
        tglTruck = v.findViewById(R.id.tglOrderTruck);
        jenisMuatanTruck = v.findViewById(R.id.jenisMuatanTruck);
        jenisTruck = v.findViewById(R.id.jenisTruck);
        jumlahTruck = v.findViewById(R.id.jmlTruck);
        lokasiBarangTruck = v.findViewById(R.id.lokasiBarangTruck);
        nmPengirimTruck = v.findViewById(R.id.namaPengirimTruck);
        alamatPengirimTruck = v.findViewById(R.id.alamatPengirimTruck);
        phonePengirimTruck = v.findViewById(R.id.phonePengirimTruck);
        nmPenerimaTruck = v.findViewById(R.id.namaPenerimaTruck);
        alamatPenerimaTruck = v.findViewById(R.id.alamatPenerimaTruck);
        phonePenerimaTruck = v.findViewById(R.id.phonePenerimaTruck);


        view1 = v.findViewById(R.id.view1Track);
        scrollKurir = v.findViewById(R.id.scroll1TrackKurir);
        scrollTruck = v.findViewById(R.id.scroll1Truck);


        inputResi = v.findViewById(R.id.inputNoId);




        Button cekResi = v.findViewById(R.id.cekResi);

        cekResi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resi = inputResi.getText().toString();

                if (resi.trim().length() > 0 ) {
                    if (connectivityManager.getActiveNetworkInfo() != null
                            && connectivityManager.getActiveNetworkInfo().isAvailable()
                            && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        checkResi(resi);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "masukan nomor resi", Toast.LENGTH_LONG).show();

                }
            }
        });

        return v;
    }

    private void checkResi(final String nresi){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, urlResi, new Response.Listener<String>() {

            boolean status = false;

            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        final String id = jObj.getString(TAG_ORDER);
                        final String pilihan = jObj.getString(TAG_PILIHAN);

                        if (pilihan.equals("INSTANT_COURIER")){


                            final String asal = jObj.getString(TAG_ASAL);
                            final String tujuan = jObj.getString(TAG_TUJUAN);
                            final String tgl = jObj.getString(TAG_TGL);
                            final String harga = jObj.getString(TAG_HARGA);
                            final String isi = jObj.getString(TAG_ISI);
                            final String berat = jObj.getString(TAG_BERAT);
                            final String lokasi = jObj.getString(TAG_LOKASI);
                            final String pengirim = jObj.getString(TAG_NMPENGIRIM);
                            final String alamatPengirim = jObj.getString(TAG_ALAMATPENGIRIM);
                            final String phonePengirim = jObj.getString(TAG_PHONEPENGIRIM);
                            final String penerima = jObj.getString(TAG_NMPENERIMA);
                            final String alamatPenerima = jObj.getString(TAG_ALAMATPENERIMA);
                            final String phonePenerima = jObj.getString(TAG_PHONEPENERIMA);

                            noResi.setText(id);
                            jenisOrder.setText(pilihan);
                            kotaAsalX.setText(asal);
                            kotaTujuanX.setText(tujuan);
                            hargaKurir.setText(harga);
                            tglKurir.setText(tgl);
                            isiKiriman.setText(isi);
                            beratKurir.setText(berat);
                            lokasiKurir.setText(lokasi);
                            nmPengirim.setText(pengirim);
                            alamatPengirimKurir.setText(alamatPengirim);
                            phonePengirimKurir.setText(phonePengirim);
                            nmPenerima.setText(penerima);
                            alamatPenerimaKurir.setText(alamatPenerima);
                            phonePenerimaKurir.setText(phonePenerima);
                        } else {
                            final String asalTruck = jObj.getString(TAG_ASAL);
                            final String tujuanTruck = jObj.getString(TAG_TUJUAN);
                            final String tglTruckX = jObj.getString(TAG_TGL);
                            final String hargaTruckX = jObj.getString(TAG_HARGA);
                            final String jenisMuatanX = jObj.getString(TAG_JENISMUATAN);
                            final String jenisTruckX = jObj.getString(TAG_JENISTRUCK);
                            final String jumlahUnitX = jObj.getString(TAG_JUMLAHUNIT);
                            final String lokasiBarangX = jObj.getString(TAG_LOKASIBARANG);
                            final String pengirimTruck = jObj.getString(TAG_NMPENGIRIM);
                            final String alamatPengirimTruckX = jObj.getString(TAG_ALAMATPENGIRIM);
                            final String phonePengirimTruckX = jObj.getString(TAG_PHONEPENGIRIM);
                            final String penerimaTruckX = jObj.getString(TAG_NMPENERIMA);
                            final String alamatPenerimaTruckX = jObj.getString(TAG_ALAMATPENERIMA);
                            final String phonePenerimaTruckX = jObj.getString(TAG_PHONEPENERIMA);

                            noResiTruck.setText(id);
                            jenisOrderTruck.setText(pilihan);
                            kotaAsalXTruck.setText(asalTruck);
                            kotaTujuanXTruck.setText(tujuanTruck);
                            hargaTruck.setText(hargaTruckX);
                            tglTruck.setText(tglTruckX);
                            jenisMuatanTruck.setText(jenisMuatanX);
                            jenisTruck.setText(jenisTruckX);
                            jumlahTruck.setText(jumlahUnitX);
                            lokasiBarangTruck.setText(lokasiBarangX);
                            nmPengirimTruck.setText(pengirimTruck);
                            alamatPengirimTruck.setText(alamatPengirimTruckX);
                            phonePengirimTruck.setText(phonePengirimTruckX);
                            nmPenerimaTruck.setText(penerimaTruckX);
                            alamatPenerimaTruck.setText(alamatPenerimaTruckX);
                            phonePenerimaTruck.setText(phonePenerimaTruckX);

                        }


                        Log.e("Successfully find location!", jObj.toString());

                        status = true;

                        Toast.makeText(getActivity().getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();


                        Runnable runnableSuccess = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();

                                if (pilihan.equals("INSTANT_COURIER")){
                                    view1.setVisibility(View.VISIBLE);
                                    scrollKurir.setVisibility(View.VISIBLE);
                                    scrollTruck.setVisibility(View.GONE);
                                } else {
                                    view1.setVisibility(View.VISIBLE);
                                    scrollKurir.setVisibility(View.GONE);
                                    scrollTruck.setVisibility(View.VISIBLE);
                                }


                            }
                        };

                        Runnable runnableFailed = new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                                Toast.makeText(getActivity(), "Maaf Resi Tidak Ditemukan", Toast.LENGTH_LONG).show();

                            }
                        };

                        if (status) {
                            handler.postDelayed(runnableSuccess,1500);
                        } else {
                            handler.postDelayed(runnableFailed, 1500);
                        }

                    } else {
                        Toast.makeText(getActivity(), "Maaf Resi Tidak Ditemukan", Toast.LENGTH_LONG).show();
                        hideDialog();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("resi", nresi);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);

    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(FragmentMainMenu,0);
        MainActivity.ma.defaultHeader();
        view1.setVisibility(View.GONE);
        scrollKurir.setVisibility(View.GONE);
        scrollTruck.setVisibility(View.GONE);
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
