package id.co.sweetmushroom.siagatransv2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.model.Armada;

public class ArmadaAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Armada> item;

    public ArmadaAdapter(Activity activity, List<Armada> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int location) {
        return item.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_armada, null);

        TextView jenisTruck = convertView.findViewById(R.id.jenisTruckSewa);

        Armada data;
        data = item.get(position);

        jenisTruck.setText(data.getJenisTruck());

        return convertView;
    }
}
