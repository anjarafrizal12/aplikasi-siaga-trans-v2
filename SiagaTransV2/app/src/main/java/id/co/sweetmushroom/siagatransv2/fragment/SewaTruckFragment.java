package id.co.sweetmushroom.siagatransv2.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.OnBackPressedListener;
import id.co.sweetmushroom.siagatransv2.R;
import id.co.sweetmushroom.siagatransv2.adapter.ArmadaAdapter;
import id.co.sweetmushroom.siagatransv2.adapter.KotaAdapter;
import id.co.sweetmushroom.siagatransv2.app.AppController;
import id.co.sweetmushroom.siagatransv2.model.Armada;
import id.co.sweetmushroom.siagatransv2.model.Kota;
import id.co.sweetmushroom.siagatransv2.utility.Server;

public class SewaTruckFragment extends Fragment implements OnBackPressedListener {

    Fragment FragmentMainMenu;
    public static TextView tvDate;

    boolean login = false;
    Handler handler = new Handler();
    private String nomorResi;

    //volley server
    int success;
    ConnectivityManager connectivityManager;

    KotaAdapter kotaAdapterAsal,kotaAdapterTujuan;
    ArmadaAdapter armadaAdapter;
    List<Kota> listKotaAsal = new ArrayList<Kota>();
    List<Kota> listKotaTujuan = new ArrayList<Kota>();
    List<Armada> listArmada = new ArrayList<Armada>();
    String jenisTruck;
    int banyakTruck;
    String ktAsal,ktTujuan;

    private String urlSewaTruck = Server.URL + "inputSewaTruck.php";
    private String urlKota = Server.URL + "pilihKota.php";
    private String urlArmada = Server.URL + "pilihArmada.php";

    private static final String TAG = SewaTruckFragment.class.getSimpleName();

    String nUname,nMail,nPhone,nAddress,nPass;
    String tag_json_object = "json_obj_req";
    public static final String TAG_KOTA = "kota";
    public static final String TAG_JENISTRUCK = "jenis_truck";
    public static final String TAG_JMLTRUCK = "banyak_truck";

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username, email, alamat, phone, passw;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    ProgressDialog pDialog;
    public final static String TAG_KIRIMAN = "isi_kiriman";
    public final static String TAG_BERAT = "berat";
    public final static String TAG_LOKASI = "lokasi";
    public final static String TAG_NPENGIRIM = "nama_pengirim";
    public final static String TAG_ALAMATPENGIRIM = "alamat_pengirim";
    public final static String TAG_KONTAKPENGIRIM = "kontak_pengirim";
    public final static String TAG_NAMAPENERIMA = "nama_penerima";
    public final static String TAG_ALAMATPENERIMA = "alamat_penerima";
    public final static String TAG_KOTATUJUAN = "kota_tujuan";
    public final static String TAG_KONTAKPENERIMA = "kontak_penerima";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";
    public final static String TAG_PASSWORD = "password";

    private EditText jmlTruck,jenisMuatan,lokasiBarang,nPengirim,alamatPengirim,kontakPengirim,namaPenerima,alamatPenerima,kontakPenerima;
    private Spinner kotaTujuan,kotaAsal,jenisTruckX;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_sewa_truck, container, false);
        FragmentMainMenu = new MainmenuFragment();

        tvDate = v.findViewById(R.id.tvDate);

        final LinearLayout linearDatePicker = v.findViewById(R.id.linearDatePicker);

        linearDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        jenisMuatan = v.findViewById(R.id.jenisMuatanSewaTruck);
        lokasiBarang = v.findViewById(R.id.alamatBarangTruck);
        nPengirim = v.findViewById(R.id.namaPengirimSewaTruck);
        alamatPengirim = v.findViewById(R.id.alamatPengirimSewaTruck);
        kontakPengirim = v.findViewById(R.id.kontakPengirimSewaTruck);
        namaPenerima = v.findViewById(R.id.namaPenerimaSewaTruck);
        alamatPenerima = v.findViewById(R.id.alamatPenerimaSewaTruck);
        kontakPenerima = v.findViewById(R.id.kontakPenerimaSewaTruck);
        jmlTruck = v.findViewById(R.id.jumlahTruckSewa);
        kotaAsal = v.findViewById(R.id.kotaAsalSewaTruck);
        kotaTujuan = v.findViewById(R.id.kotaTujuanSewaTruck);
        jenisTruckX = v.findViewById(R.id.spinnerTruck);

        kotaAsal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktAsal = listKotaAsal.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        kotaTujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ktTujuan = listKotaTujuan.get(i).getNmKota();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        jenisTruckX.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                jenisTruck = listArmada.get(i).getJenisTruck();
                banyakTruck = Integer.parseInt(listArmada.get(i).getBanyakTruck());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(MainActivity.session_status, true);
        nUname = sharedpreferences.getString(TAG_USERNAME, null);
        nMail = sharedpreferences.getString(TAG_EMAIL, null);
        nPhone = sharedpreferences.getString(TAG_PHONE, null);
        nAddress = sharedpreferences.getString(TAG_ALAMAT, null);
        nPass = sharedpreferences.getString(TAG_PASSWORD,null);

        kotaAdapterAsal = new KotaAdapter(getActivity(), listKotaAsal);
        kotaAdapterTujuan = new KotaAdapter(getActivity(), listKotaTujuan);
        armadaAdapter = new ArmadaAdapter(getActivity(), listArmada);

        kotaAsal.setAdapter(kotaAdapterAsal);
        kotaTujuan.setAdapter(kotaAdapterTujuan);
        jenisTruckX.setAdapter(armadaAdapter);

        fetchArmada();
        fetchKota();


        Button pesanTruck = v.findViewById(R.id.bookingTruck);

        pesanTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String jenisMuatanX = jenisMuatan.getText().toString();
                String lokasiBarangX = lokasiBarang.getText().toString();
                String nPengirimX = nPengirim.getText().toString();
                String alamatPengirimX = alamatPengirim.getText().toString();
                String kontakPengirimX = kontakPengirim.getText().toString();
                String namaPenerimaX = namaPenerima.getText().toString();
                String alamatPenerimaX = alamatPenerima.getText().toString();
                String kontakPenerimaX = kontakPenerima.getText().toString();
                String jumlahTruck = jmlTruck.getText().toString();

                hideKeyboard(getContext());

                // mengecek kolom yang kosong
                if (jenisMuatanX.trim().length() > 0 && lokasiBarangX.trim().length() > 0 && nPengirimX.trim().length() > 0
                        && alamatPengirimX.trim().length() > 0 && kontakPengirimX.trim().length() > 0 && namaPenerimaX.trim().length() > 0
                        && alamatPenerimaX.trim().length() > 0 && jenisTruck.trim().length() > 0 && kontakPenerimaX.trim().length() > 0 && ktAsal.trim().length() > 0 && ktTujuan.trim().length() > 0
                        && jumlahTruck.trim().length() > 0) {
                    int jumlahTruckBaru = Integer.parseInt(jumlahTruck);
                    if (connectivityManager.getActiveNetworkInfo() != null
                            && connectivityManager.getActiveNetworkInfo().isAvailable()
                            && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        if (jumlahTruckBaru <= banyakTruck){
                            inputSewaTruck(jenisMuatanX,lokasiBarangX,nPengirimX,jenisTruck,alamatPengirimX,kontakPengirimX,namaPenerimaX,alamatPenerimaX,kontakPenerimaX,ktAsal,ktTujuan,jumlahTruck);
                        } else {
                            Toast.makeText(getActivity().getApplicationContext() ,"Maaf Jumlah Truck yang dipesan Melebihi batas Jumlah Armada", Toast.LENGTH_LONG).show();
                            Toast.makeText(getActivity().getApplicationContext() ,"Jumlah Armada "+jenisTruck+ " yang tersedia : "+banyakTruck, Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(getActivity().getApplicationContext() ,"No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity(), "Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
                }

            }
        });


        return v;
    }

    @Override
    public boolean onBackPressed() {
        MainActivity.ma.setFragment(FragmentMainMenu,0);
        MainActivity.ma.defaultHeader();
        return true;
    }

    private void inputSewaTruck(final String jenisMuatan,final String lokasiBarangX,final String nPengirimX,
                                final String jenisTruck,final String alamatPengirimX,final String kontakPengirimX,
                                final String namaPenerimaX,final String alamatPenerimaX,final String kontakPenerimaX,
                                final String kotaAsal,final String kotaTujuan,final String jumlahTruck){

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Pesan Truck ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, urlSewaTruck, new Response.Listener<String>() {

            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {

                        Log.e("Berhasil input sewa Truck!", jObj.toString());

                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        clearText();
                        nomorResi = jObj.getString("resi");

                        showDialogPemberitahuan(jenisMuatan,jenisTruck,jumlahTruck,lokasiBarangX,nPengirimX,alamatPengirimX,kontakPengirimX,namaPenerimaX,
                                alamatPenerimaX,kontakPenerimaX,kotaAsal,kotaTujuan);

                        //showDialogPemberitahuan(kiriman,beratBarang,lokasiBarang,namaPengirim,alamatPengirim,kontakPengirim,namaPenerima,
                                //alamatPenerima,kotaAsal,kotaTujuan,kontakPenerima);


                    } else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Input Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",nUname);
                params.put("jenis_muatan", jenisMuatan);
                params.put("lokasi_barang", lokasiBarangX);
                params.put("nama_pengirim", nPengirimX);
                params.put("jenis_truck", jenisTruck);
                params.put("alamat_pengirim", alamatPengirimX);
                params.put("kontak_pengirim", kontakPengirimX);
                params.put("nama_penerima", namaPenerimaX);
                params.put("alamat_penerima", alamatPenerimaX);
                params.put("kota_asal", kotaAsal);
                params.put("kota_tujuan", kotaTujuan);
                params.put("kontak_penerima", kontakPenerimaX);
                params.put("jumlah_truck",jumlahTruck);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_object);
    }

    private void clearText(){
    }

    //hide keyboard
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }


        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            tvDate.setText(day+"/"+month+"/"+year);
        }

    }

    private void fetchArmada(){
        listArmada.clear();

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");

        // Creating volley request obj
        JsonArrayRequest jArrRequest = new JsonArrayRequest(urlArmada,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        Armada armada = new Armada();
                        armada.setJenisTruck("Pilih Jenis Truck");
                        armada.setBanyakTruck("0");
                        listArmada.add(armada);

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Armada item = new Armada();

                                item.setJenisTruck(obj.getString(TAG_JENISTRUCK));
                                item.setBanyakTruck(obj.getString(TAG_JMLTRUCK));

                                listArmada.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        armadaAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArrRequest);
    }

    private void fetchKota(){
        listKotaAsal.clear();
        listKotaTujuan.clear();

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");
        showDialog();

        // Creating volley request obj
        JsonArrayRequest jArrRequest = new JsonArrayRequest(urlKota,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        Kota awal = new Kota();
                        awal.setNmKota("Pilih Kota Asal");
                        listKotaAsal.add(awal);

                        Kota tujuan = new Kota();
                        tujuan.setNmKota("Pilih Kota Tujuan");
                        listKotaTujuan.add(tujuan);

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Kota item = new Kota();

                                item.setNmKota(obj.getString(TAG_KOTA));

                                listKotaAsal.add(item);
                                listKotaTujuan.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        kotaAdapterAsal.notifyDataSetChanged();
                        kotaAdapterTujuan.notifyDataSetChanged();

                        hideDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jArrRequest);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showDialogPemberitahuan(final String jenisMuatan,final String jenisTruck, final String jumlahTruck, final String lokasiBarang, final String namaPengirim,
                                         final String alamatPengirim,final String kontakPengirim, final String namaPenerima,final String alamatPenerima,final String kontakPenerima,final String kotaAsal,final String kotaTujuan){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_pemberitahuan);
        dialog.setTitle("INFO RESI!");
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final ScrollView scrollTruck = dialog.findViewById(R.id.sewaTruck);
        TextView resiX = dialog.findViewById(R.id.nomorResiSewaTruck);
        TextView jenisMuatanX = dialog.findViewById(R.id.jenisMuatanDialog);
        TextView jenisTruckX = dialog.findViewById(R.id.jenisTruckDialog);
        TextView jumlahTruckX = dialog.findViewById(R.id.jumlahTruckDialog);
        TextView lokasiBarangX = dialog.findViewById(R.id.lokasiBarangTruckDialog);
        TextView namaPengirimX = dialog.findViewById(R.id.namaPengirimTruckDialog);
        TextView alamatPengirimX = dialog.findViewById(R.id.alamatPengirimTruckDialog);
        TextView kontakPengirimX = dialog.findViewById(R.id.kontakPengirimTruckDialog);
        TextView namaPenerimaX = dialog.findViewById(R.id.namaPenerimaTruckDialog);
        TextView alamatPenerimaX = dialog.findViewById(R.id.alamatpenerimaTruckDialog);
        TextView kontakPenerimaX = dialog.findViewById(R.id.kontakPenerimaTruckDialog);
        TextView kotaPengirimX = dialog.findViewById(R.id.kotaAsalTruckDialog);
        TextView kotaPenerimaX = dialog.findViewById(R.id.kotaTujuanTruckDialog);

        Button btnOkTruck = dialog.findViewById(R.id.btnOkTruckDialog);

        resiX.setText(nomorResi);
        jenisMuatanX.setText(jenisMuatan);
        jenisTruckX.setText(jenisTruck);
        jumlahTruckX.setText(jumlahTruck);
        lokasiBarangX.setText(lokasiBarang);
        namaPengirimX.setText(namaPengirim);
        alamatPengirimX.setText(alamatPengirim);
        kotaPengirimX.setText(kotaAsal);
        kontakPengirimX.setText(kontakPengirim);
        namaPenerimaX.setText(namaPenerima);
        alamatPenerimaX.setText(alamatPenerima);
        kotaPenerimaX.setText(kotaTujuan);
        kontakPenerimaX.setText(kontakPenerima);

        scrollTruck.setVisibility(View.VISIBLE);

        btnOkTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                scrollTruck.setVisibility(View.GONE);
                MainActivity.ma.setFragment(FragmentMainMenu,0);
                MainActivity.ma.defaultHeader();

            }
        });

        dialog.show();

    }

}
