package id.co.sweetmushroom.siagatransv2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import id.co.sweetmushroom.siagatransv2.fragment.AboutUsFragment;
import id.co.sweetmushroom.siagatransv2.fragment.HelpFragment;
import id.co.sweetmushroom.siagatransv2.fragment.MainmenuFragment;
import id.co.sweetmushroom.siagatransv2.fragment.ProfileFragment;
import id.co.sweetmushroom.siagatransv2.fragment.SettingFragment;
import id.co.sweetmushroom.siagatransv2.fragment.SignInFragment;

public class MainActivity extends AppCompatActivity {

    //Fragment
    private Fragment mainmenu, signIn, profile, setting, help, aboutUs;

    //Variabel Session
    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username, email,phone;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";

    //Variabel MainActivity
    private OnBackPressedListener onBackPressedListener;
    public static MainActivity ma;
    private TextView textMenu, textJudul;
    private ImageView arrow, ivHeader;
    private int i = 0;
    private boolean statusLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMain);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBlack)); //status bar or the time bar at the top
        }

        ma = this;

        textMenu = findViewById(R.id.textMenu);
        textJudul = findViewById(R.id.textJudul);
        arrow = findViewById(R.id.back);

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultHeader();
            }
        });


        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        ImageView ivHeader = header.findViewById(R.id.imageView);
        ivHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session) {
                    setFragment(profile, 1);
                    setHeader("PROFILE");

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.END);

                } else {
                    setFragment(signIn, 1);
                    setHeader("SIGN IN");

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.END);

                }
            }
        });

        //Inisialisasi Fragment
        mainmenu = new MainmenuFragment();
        signIn = new SignInFragment();
        profile = new ProfileFragment();
        setting = new SettingFragment();
        help = new HelpFragment();
        aboutUs = new AboutUsFragment();

        //Set Fragment Awal
        setFragment(mainmenu, 2);

        //Handle Session
        sharedpreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(session_status, false);
        id = sharedpreferences.getString(TAG_ID, null);
        email = sharedpreferences.getString(TAG_EMAIL, null);
        username = sharedpreferences.getString(TAG_USERNAME, null);
        phone = sharedpreferences.getString(TAG_PHONE, null);
        email = sharedpreferences.getString(TAG_EMAIL, null);

        checkStatusLogin();

        displayRightNavigation();
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (!(fragment instanceof OnBackPressedListener) || !((OnBackPressedListener) fragment).onBackPressed()) {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            drawer.openDrawer(GravityCompat.END);
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayRightNavigation() {
        final NavigationView navigationViewRight = (NavigationView) findViewById(R.id.nav_view);
        navigationViewRight.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.profile) {
                    if (statusLogin == false) {
                        setFragment(signIn, 1);
                        setHeader("SIGN IN");
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.END);
                    } else {
                        setFragment(profile, 1);
                        setHeader("PROFILE");
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.END);
                    }

                } else if (id == R.id.setting) {
                    setFragment(setting, 1);
                    setHeader("SETTING");
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.END);

                } else if (id == R.id.help) {
                    setFragment(help, 1);
                    setHeader("HELP");
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.END);

                } else if (id == R.id.about) {
                    setFragment(aboutUs, 1);
                    setHeader("ABOUT US");
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.END);

                } else if (id == R.id.signout) {
                    if (session) {
                        alertDialog();
                    }

                }

                return true;

            }
        });
    }

    public void setFragment(Fragment fragment, int i) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (i == 0) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left_invers, R.anim.slide_in_rigth_invers);
        } else if (i == 1) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
        }

        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    public void setHeader(String judul) {
        textMenu.setVisibility(View.GONE);
        arrow.setVisibility(View.VISIBLE);
        textJudul.setText(judul);
    }

    public void defaultHeader() {
        arrow.setVisibility(View.GONE);
        textMenu.setVisibility(View.VISIBLE);
        textJudul.setText("SIAGA TRANS");
        setFragment(mainmenu, 0);
    }

    public void changeStatusLogin() {
        statusLogin = true;
        session = sharedpreferences.getBoolean(MainActivity.session_status, true);
        email = sharedpreferences.getString(TAG_EMAIL, null);
        username = sharedpreferences.getString(TAG_USERNAME, null);
    }

    public void clearSession() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(MainActivity.session_status, false);
        editor.putString(TAG_ID, null);
        editor.putString(TAG_USERNAME, null);
        editor.apply();

        statusLogin = false;
        session = sharedpreferences.getBoolean(MainActivity.session_status, false);
        email = sharedpreferences.getString(TAG_EMAIL, null);
        username = sharedpreferences.getString(TAG_USERNAME, null);
    }

    public void checkStatusLogin() {
        if (session) {
            statusLogin = true;

            NavigationView navigationView = findViewById(R.id.nav_view);
            View header = navigationView.getHeaderView(0);
            Menu mn = navigationView.getMenu();

            final ImageView ivHeader = header.findViewById(R.id.imageView);
            final TextView tv_nama_header = header.findViewById(R.id.tv_nama);
            final TextView tv_email_header = header.findViewById(R.id.tv_email);
            final MenuItem signUp = mn.findItem(R.id.profile);
            final MenuItem setting = mn.findItem(R.id.setting);
            final MenuItem signOut = mn.findItem(R.id.signout);

            //set textview header
            tv_nama_header.setVisibility(View.VISIBLE);
            tv_email_header.setVisibility(View.VISIBLE);
            tv_nama_header.setText(username);
            tv_email_header.setText(email);
            ivHeader.setImageResource(R.drawable.person);

            signUp.setTitle("Profile");
            signUp.setIcon(R.drawable.ic_person);
            setting.setVisible(true);
            signOut.setVisible(true);

        } else {
            statusLogin = false;

            NavigationView navigationView = findViewById(R.id.nav_view);
            View header = navigationView.getHeaderView(0);
            Menu mn = navigationView.getMenu();

            final ImageView ivHeader = header.findViewById(R.id.imageView);
            final TextView tv_nama_header = header.findViewById(R.id.tv_nama);
            final TextView tv_email_header = header.findViewById(R.id.tv_email);
            final MenuItem signUp = mn.findItem(R.id.profile);
            final MenuItem setting = mn.findItem(R.id.setting);
            final MenuItem signOut = mn.findItem(R.id.signout);

            tv_nama_header.setVisibility(View.INVISIBLE);
            tv_email_header.setVisibility(View.INVISIBLE);
            tv_nama_header.setText("");
            tv_email_header.setText("");
            ivHeader.setImageResource(R.drawable.add_person);

            signUp.setTitle("Sign In");
            setting.setVisible(false);
            signOut.setVisible(false);

        }
    }

    private void alertDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Sign Out")
                .setMessage("Are you sure want to sign out?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearSession();

                        checkStatusLogin();
                        dialog.dismiss();
                        setFragment(mainmenu, 0);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.END);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.END);
                    }
                }).show();
    }


}
