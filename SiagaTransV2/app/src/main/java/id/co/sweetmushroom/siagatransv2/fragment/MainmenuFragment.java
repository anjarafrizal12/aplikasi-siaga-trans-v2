package id.co.sweetmushroom.siagatransv2.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import id.co.sweetmushroom.siagatransv2.MainActivity;
import id.co.sweetmushroom.siagatransv2.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainmenuFragment extends Fragment {

    private Fragment fragmentCourier;
    private Fragment sewaTruck;
    private Fragment cekHarga;
    private Fragment trackPacket;
    private Fragment signIn;

    SharedPreferences sharedpreferences;
    Boolean session = false;
    String id, username, email,phone;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";
    public final static String TAG_EMAIL = "email";
    public final static String TAG_PHONE = "phone";
    public final static String TAG_ALAMAT = "alamat";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_menu, container, false);

        ImageView ivCourier   = v.findViewById(R.id.ivCourier);
        ImageView ivTruck     = v.findViewById(R.id.ivTruck);
        ImageView ivHarga     = v.findViewById(R.id.ivPrice);
        ImageView ivRoute     = v.findViewById(R.id.ivRoute);

        sharedpreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(session_status, false);
        id = sharedpreferences.getString(TAG_ID, null);
        email = sharedpreferences.getString(TAG_EMAIL, null);
        username = sharedpreferences.getString(TAG_USERNAME, null);
        phone = sharedpreferences.getString(TAG_PHONE, null);
        email = sharedpreferences.getString(TAG_EMAIL, null);

        fragmentCourier = new CourierFragment();
        sewaTruck       = new SewaTruckFragment();
        cekHarga        = new CekHargaFragment();
        trackPacket     = new TrackPacketFragment();
        signIn          = new SignInFragment();


        ivCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session) {
                    MainActivity.ma.setHeader("INSTANT COURIER");
                    MainActivity.ma.setFragment(fragmentCourier, 1);
                } else {
                    MainActivity.ma.setFragment(signIn, 1);
                    MainActivity.ma.setHeader("SIGN IN");
                }
            }
        });

        ivTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session) {
                    MainActivity.ma.setHeader("SEWA TRUCK");
                    MainActivity.ma.setFragment(sewaTruck, 1);
                } else {
                    MainActivity.ma.setFragment(signIn, 1);
                    MainActivity.ma.setHeader("SIGN IN");
                }
            }
        });

        ivHarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.ma.setHeader("CEK HARGA");
                MainActivity.ma.setFragment(cekHarga,1);
            }
        });

        ivRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.ma.setHeader("TRACK PACKET");
                MainActivity.ma.setFragment(trackPacket,1);
            }
        });

        return v;
    }

}
